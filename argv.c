#include "argv.h"

Option* init_options() {
    Option *options = malloc(sizeof(*options));
    if (options == NULL) { 
        fprintf(stderr, "[ERROR] Impossible to create options\n");
        exit(EXIT_FAILURE);
    }
    options->mode = 0;
    options->proper_mode = 0;
    options->alea_mode = 0;
    options->rate_mode = 0;
    options->input = NULL;
    options->output = NULL;
} 

void option_parser(int argc, char *argv[], Option *options) {
	int option, mode, proper_mode = 0, alea_mode = 0, rate_mode = 0;
    unsigned char flag = 0;
    char *input = NULL, *output = NULL; 
    if (argc < 3) { 
        printf("Usage :\n   Insertion : ./jpeg_cpy -i input_jpg -o output_jpg\n   Extraction : ./jpeg_cpy -e input_jpg\n   -r : to set a rate of insertion(between 0 and 1)\n   -p : to a proper insertion/extraction\n   -a : to an alea insertion/extraction\n   -h : to make an histograme of DCT between two values\n");
        exit(EXIT_FAILURE);
    }

    while((option = getopt( argc, argv, "e:i:o:rpah")) != -1) {
        switch(option) {
            case 'e':
                flag |= EXTRACT;
                input = optarg;
                printf("Extract %s\n", optarg);
                break;
            case 'i':
                printf("Input %s\n", optarg);
                flag |= INPUT;
                input = optarg;
                break;
            case 'o':
                printf("Output %s\n", optarg);
                flag |= OUTPUT;
                output = optarg;
                break;
            case 'r':
                printf("Rate mode activated\n");
                flag |= RATE;
                rate_mode = RATE_MODE;
                break;
            case 'p':
                printf("Proper mode activated\n");
                proper_mode = PROPER_MODE;
                break;
            case 'a':
                printf("Alea mode activated\n");
                alea_mode = ALEA_MODE;
                break;
            case 'h':
                printf("histo mode\n");
                flag |= HISTO;
                break;
            case '?':
                if (option == 'i') { 
                    fprintf(stderr, "Option -%c requires arguments\n", optopt);
                }
                else if (option == 'o') { 
                    fprintf(stderr, "Option -%c requires arguments\n", optopt);
                }
                else if (option == 'e') { 
                    fprintf(stderr, "Option -%c requires arguments\n", optopt);
                }
                else if(isprint(optopt))
                    fprintf(stderr, "Unknown option -%c\n", optopt);
                else
                    fprintf(stderr, "Unknown option character `\\x%x'.\n", optopt);
            default:
                printf("Usage :\n   Insertion : ./jpeg_cpy -i input_jpg -o output_jpg\n   Extraction : ./jpeg_cpy -e input_jpg\n   -r : to set a rate of insertion(between 0 and 1)\n   -p : to a proper insertion/extraction\n   -a : to an alea insertion/extraction\n   -h : to make an histograme of DCT between two values\n");
                exit(EXIT_FAILURE);
        }
    }
    if ((mode = options_validation(flag)) != ERROR) { 
        assign_options(options, mode, proper_mode, alea_mode, rate_mode, input, output);
    }
    else { 
        exit(EXIT_FAILURE);
    }
}

int options_validation(unsigned char flag) {
    if (flag != 0) {
    	if ((flag & INPUT) != 0 && (flag & HISTO) != 0) { 
            return HISTO_MODE;
        }
        else if ((flag & INPUT) != 0 && (flag & OUTPUT) != 0 && (flag & EXTRACT) == 0 && (flag & HISTO) == 0) { 
            return STEGANO_MODE;
        }
        else if ((flag & INPUT) == 0 && (flag & OUTPUT) == 0 && (flag & EXTRACT) != 0 && (flag & RATE) == 0 && (flag & HISTO) == 0) { 
            return EXTRACT_MODE;
        }
        else { 
            fprintf(stderr, "[ERROR] Incompatible options\n");
            return ERROR;
        }
    }
} 

void assign_options(Option *options, int mode, int proper_mode,int alea_mode,int rate_mode, char *input, char *output) {
    options->mode = mode;
    options->proper_mode = proper_mode;
    options->alea_mode = alea_mode;
    options->rate_mode = rate_mode;

    if (options->mode == STEGANO_MODE) { 
        options->output = malloc(sizeof(char)*strlen(output));
        options->output = output;
    }

    options->input = malloc(sizeof(char)*strlen(input));
    options->input = input;
} 
