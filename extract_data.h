#ifndef EXTRACT_DATA_4XZ0GYPD

#define EXTRACT_DATA_4XZ0GYPD

#include <stdio.h>
#include <stdlib.h>
#include "jpeg_stegano.h"
#include "file.h"

#define NB_COMPONENT 3
#define NB_DCT_ELEMENT 64

void extract_lsb_data(JPEGimg *img, Option *options);
void extract_lsb_data_with_key(JPEGimg *img, Option *options);


#endif /* end of include guard: EXTRACT_DATA_4XZ0GYPD */
