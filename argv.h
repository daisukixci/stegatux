#ifndef ARGV_PUOJMJS2

#define ARGV_PUOJMJS2

#include <stdio.h>
#include <unistd.h>
#include <ctype.h>
#include <string.h>
#include <stdlib.h>

#define EXTRACT_MODE 0
#define STEGANO_MODE 1
#define PROPER_MODE 2
#define ALEA_MODE 3
#define RATE_MODE 4
#define HISTO_MODE 5

#define ERROR -1

#define INPUT 1
#define OUTPUT 2
#define EXTRACT 4
#define PROPER 8
#define ALEA 16
#define RATE 32
#define HISTO 64

typedef struct _Option {
    int mode;
    int proper_mode;
    int alea_mode;
    int rate_mode;
    char *input;
    char *output;
} Option;

Option* init_options();
void option_parser(int argc, char *argv[], Option *options);
int options_validation(unsigned char flag);
void assign_options(Option *options, int mode, int proper_mode, int alea_mode, int rate_mode, char *input, char *output);

#endif /* end of include guard: ARGV_PUOJMJS2 */
