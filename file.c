#include "file.h"

FILE * open_file(char *url, char *mode) {
    FILE *file = fopen(url, mode);

    /*
     * Open file and verify it worked else exit the program
     */
    if (file == NULL) { 
        fprintf(stderr, "[ERROR] impossible to open the file: %s\n", url);
        exit(EXIT_FAILURE);
    }
    return file;
}

void close_file(FILE *file){
    /*
     * Close the file and put the pointer to NULL
     */
    fclose(file);
    file = NULL;
    if (file != NULL) { 
        fprintf(stderr, "[ERROR] file haven't been closed\n");
    }
}

void read_line_in_file(FILE *file, char *line){
    /*
     * Read one line and clean it of \n put by the fgets
     */
    if (fgets(line, MAX_LINE_LENGHT, file) != NULL) {
        clear_string(line);
    }
} 

void read_file(FILE *file, char *text) {
    int i = 0;
    int c;

    while ((c = fgetc(file)) != EOF) {
        text[i] = (char)c;
        i ++;
    }
}

void clear_string(char *string){
    unsigned int i;
    /*
     * Clean a string of \n put by the fgets
     */
    for (i = 0; i < strlen(string); i++) {
        if (string[i] == '\n') { 
            string[i] = '\0';
        }
        else if (string[i] == '\0') { 
            break;
        }
    }
} 

void write_in_file(char *input, int size, char *filename) {
    int i = 0;
    FILE *output_file = NULL;

    output_file = fopen(filename, "w+");
    if (output_file == NULL) { 
        fprintf(stderr, "Impossible to open the file %s\n", filename);
        exit(EXIT_FAILURE);
    }
    for (i = 0; i < size; i++) {
        fputc(input[i], output_file);
    }

    fclose(output_file);
    output_file = NULL;
} 

