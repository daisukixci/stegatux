#ifndef INSERTION_12345678
#define INSERTION_12345678

#include <stdio.h>
#include <stdlib.h>

#include "error.h"
#include "jpeg_stegano.h"

#endif

//fonction d'insertion débile dans l'image
int insertion (JPEGimg *img, int mode, int rate);
int insertion_cle (JPEGimg *img, int mode, int rate);
