#ifndef JPEG_STEGANO_H_
#define JPEG_STEGANO_H_

//#include <jpeglib.h>
#include <stdlib.h>
#include <stdio.h>
#include <time.h>

#include "error.h"
#include "argv.h"
#include "jpeg-8/cdjpeg.h"		/* Common decls for cjpeg/djpeg applications */
#include "jpeg-8/jversion.h"		/* for version message */

#define MAX_DCT_VALUE (255*32)
#define MIN_DCT_VALUE (-255*32)
#define NB_DCT_COEFFS (MAX_DCT_VALUE - MIN_DCT_VALUE + 1)

//typedef struct jpeg_decompress_struct sjdec;

/* Structure of main informations of a JPEG image */
typedef struct JPEGimg_s
{
	JBLOCKARRAY * dctCoeffs;
	struct jpeg_decompress_struct cinfo;
	jvirt_barray_ptr * virtCoeffs;
} JPEGimg;

/******************** FOCNTIONS of jpeg_stegano.c ****************************/

JPEGimg *init_jpeg_img ( void );
int free_jpeg_img ( JPEGimg *img );

JPEGimg * jpeg_read (Option *options);

int jpeg_write_from_coeffs (Option *options, JPEGimg *img);
                            
int write_histo (JPEGimg *img);
#endif /*JPEG_STEGANO_H_*/
