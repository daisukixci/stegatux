#ifndef FILE_D2B6850X

#define FILE_D2B6850X

#include <stdio.h>
#include <stdlib.h>
#include <string.h>

#define MAX_LINE_LENGHT 2000

FILE *open_file(char *url, char *mode);
void close_file(FILE *file);
void read_line_in_file(FILE *file, char *line);
void clear_string(char *string);
void read_file(FILE *file, char *text);
void write_in_file(char *input, int size, char *filename);

#endif /* end of include guard: FILE_D2B6850X */
