#include "insertion.h"


/*
 * Insertion sans alea
 */
int insertion (JPEGimg *img, int mode, int rate){
	int count, retour, fileSize, nb_dct, nb_dct_total;
	int i,j,k,l;
	double rate_value;
	unsigned char * message = NULL;
	unsigned char lettre, value;
	FILE *file_message = NULL;
	
	
	file_message = fopen("text","r");
	
	fseek(file_message, 0, SEEK_END);
	fileSize = ftell(file_message);
	rewind(file_message);
	
	//comptage du nombre total de dct
	nb_dct = 0;
	nb_dct_total = 0;
	for(i=0;i<3;i++){
		nb_dct += img->cinfo.comp_info[i].height_in_blocks * img->cinfo.comp_info[i].width_in_blocks * 64;
		nb_dct_total += img->cinfo.comp_info[i].height_in_blocks * img->cinfo.comp_info[i].width_in_blocks * 64;
		if(mode == PROPER_MODE)
			nb_dct -= img->cinfo.comp_info[i].height_in_blocks * img->cinfo.comp_info[i].width_in_blocks;
	}
	
	count = 0;
	if(mode == PROPER_MODE)
		for (i=0; i<3; i++)
			for (j=0; j<img->cinfo.comp_info[i].height_in_blocks; j++)
				for (k=0; k<img->cinfo.comp_info[i].width_in_blocks; k++)
					for (l=1; l<64; l++)
						if(img->dctCoeffs[i][j][k][l] == 0 || img->dctCoeffs[i][j][k][l] == 1 /*|| img->dctCoeffs[i][j][k][l] == -1*/)
							nb_dct -= 1;
	
	if((fileSize+4)*8 > nb_dct)
		return 1;
	if(rate == RATE_MODE){
		printf("type the rate value (between 0 and 1) : ");
		retour = scanf("%lf", &rate_value);
		if(nb_dct*rate_value < (fileSize+4)*8)
			return 1;
	}
	
	if( (message = malloc((fileSize+4) * sizeof(unsigned char))) == NULL )
		printf("error malloc message\n");
	message[0] = (fileSize >> 24) & 0xFF;
	message[1] = (fileSize >> 16) & 0xFF;
	message[2] = (fileSize >> 8) & 0xFF;
	message[3] = (fileSize) & 0xFF;
	count = 4;
	
	while(retour = fscanf(file_message,"%c",&lettre), !feof(file_message)){
		message[count] = lettre;
		count++;
	}
	
	fclose(file_message);
				
	count = 0;
	for (i=0; i<3; i++){
		for (j=0; j<img->cinfo.comp_info[i].height_in_blocks; j++){
			for (k=0; k<img->cinfo.comp_info[i].width_in_blocks; k++){
				for (l=1; l<64; l++){
					if(mode == PROPER_MODE){
						if(img->dctCoeffs[i][j][k][l] != 0 && img->dctCoeffs[i][j][k][l] != 1 /*&& img->dctCoeffs[i][j][k][l] != -1*/){
							img->dctCoeffs[i][j][k][l] &= 0xFFFFFFFE;
							value = message[count/8];
							value = (value >> (7-(count%8))) & 1;
							//printf("%d", value);
							img->dctCoeffs[i][j][k][l] += value;
							//if(count%8 == 7)
							//	printf(" -> %d\t-> '%c'\n",message[count/8], message[count/8]);
					
							count ++;
							if(count == ((fileSize+4)*8)){
								printf("Taille du texte : \t\t%d\nNombre de DCT modifiable : \t%d\nNombre de DCT total : \t\t%d\nNombre de DCT modifé : \t\t%d/%d\nTaux d'insertion : \t\t%f\n",(message[0]<<24)+(message[1]<<16)+(message[2]<<8)+message[3], nb_dct, nb_dct_total, count, nb_dct, (double)count/nb_dct);
								free(message);
								return 0;
							}
						}
					}
					else {
						img->dctCoeffs[i][j][k][l] &= 0xFFFFFFFE;
						value = message[count/8];
						value = (value >> (7-(count%8))) & 1;
				
						//printf("%d", value);
						img->dctCoeffs[i][j][k][l] += value;
		                //if(count%8 == 7)
                 	    //   printf(" -> %d\t-> '%c'\n",message[count/8], message[count/8]);
					
						count ++;
						if(count == ((fileSize+4)*8)){
							printf("Taille du texte : \t\t%d\nNombre de DCT modifiable : \t%d\nNombre de DCT total : \t\t%d\nNombre de DCT modifé : \t\t%d/%d\nTaux d'insertion : \t\t%f\n",(message[0]<<24)+(message[1]<<16)+(message[2]<<8)+message[3], nb_dct, nb_dct_total, count, nb_dct, (double)count/nb_dct);
							free(message);
							return 0;
						}
					}
				}
			}
		}
	}
	printf("Taille du texte : \t\t%d\nNombre de DCT modifiable : \t%d\nNombre de DCT total : \t\t%d\nNombre de DCT modifé : \t\t%d/%d\nTaux d'insertion : \t\t%f\n",
		(message[0]<<24)+(message[1]<<16)+(message[2]<<8)+message[3], nb_dct, nb_dct_total, count, nb_dct, (double)count/nb_dct);
	free(message);
	return 0;
}

/*
 * Insertion avec alea
 */

int insertion_cle (JPEGimg *img, int mode, int rate){
	int count, retour, fileSize, nb_dct,nb_dct_total, alea, valid, count_pos;
	int i,j,k,l,m;
	double rate_value;
	unsigned int cle;
	unsigned char * message = NULL;
	unsigned int * tab_alea = NULL;
	unsigned char lettre, value;
	FILE *file_message = NULL;
	
	
	file_message = fopen("text","r");
	
	fseek(file_message, 0, SEEK_END);
	fileSize = ftell(file_message);
	rewind(file_message);
	
	//comptage du nombre total de dct
	nb_dct = 0;
	nb_dct_total = 0;
	for(i=0;i<3;i++){
		nb_dct += img->cinfo.comp_info[i].height_in_blocks * img->cinfo.comp_info[i].width_in_blocks * 64;
		nb_dct_total += img->cinfo.comp_info[i].height_in_blocks * img->cinfo.comp_info[i].width_in_blocks * 64;
		if(mode == PROPER_MODE)
			nb_dct -= img->cinfo.comp_info[i].height_in_blocks * img->cinfo.comp_info[i].width_in_blocks;
	}
	
	count = 0;
	if(mode == PROPER_MODE)
		for (i=0; i<3; i++)
			for (j=0; j<img->cinfo.comp_info[i].height_in_blocks; j++)
				for (k=0; k<img->cinfo.comp_info[i].width_in_blocks; k++)
					for (l=1; l<64; l++)
						if(img->dctCoeffs[i][j][k][l] == 0 || img->dctCoeffs[i][j][k][l] == 1 /*|| img->dctCoeffs[i][j][k][l] == -1*/)
							nb_dct -= 1;
	
	if((fileSize+4)*8 > nb_dct)
		return 1;
	if(rate == RATE_MODE){
		printf("type the rate value (between 0 and 1) : ");
		retour = scanf("%lf", &rate_value);
		if(nb_dct*rate_value < (fileSize+4)*8)
			return 1;
	}
	
	if( (message = malloc((fileSize+4) * sizeof(unsigned char))) == NULL )
		printf("error malloc message\n");
	message[0] = (fileSize >> 24) & 0xFF;
	message[1] = (fileSize >> 16) & 0xFF;
	message[2] = (fileSize >> 8) & 0xFF;
	message[3] = (fileSize) & 0xFF;
	count = 4;
	
	while(retour = fscanf(file_message,"%c",&lettre), !feof(file_message)){
		message[count] = lettre;
		count++;
	}
	
	fclose(file_message);
	
/********** partie ou l'alea est géré *********/
	//saisi de la cle
	printf("type the key (only positive numbers) : ");
	retour = scanf("%d", &cle);
	//initialisation de la suite aleatoire en fonction de la cle
	srand(cle);
	
	//initialisation du tableau contenant les positions des dct modifié afin d'éviter les ecrasements de données
	if( (tab_alea = malloc((fileSize+4)*8 * sizeof(unsigned int))) == NULL )
		printf("error malloc tab_alea\n");
		
	count = 0;
	while(count < (fileSize+4)*8){
		
		
		//verification anti doublon
        do{
            valid = 0;
            alea = rand()%(nb_dct_total+1);
            tab_alea[count] = alea;
            for(m=0;m<count;m++)
                if(tab_alea[m] == alea)
                    valid++;
        }while(valid > 0);

        //definition de la position par rapport a la valeur aléatoire
        count_pos = 0;
        for (i=0; i<3; i++){
			for (j=0; j<img->cinfo.comp_info[i].height_in_blocks; j++){
				for (k=0; k<img->cinfo.comp_info[i].width_in_blocks; k++){
					for (l=0; l<64; l++){
						if(count_pos == alea){
							goto out;
						}
						count_pos ++;
					}
				}
			}
		}
		out:
		//printf("%d -- %d\n", alea, img->dctCoeffs[i][j][k][l]);
/**********************************************/

        //insertion dans l'image en fonction du mode choisi
        if(mode == PROPER_MODE){
            if(img->dctCoeffs[i][j][k][l] != 0 && img->dctCoeffs[i][j][k][l] != 1 /*&& img->dctCoeffs[i][j][k][l] != -1*/ && l != 0){
                img->dctCoeffs[i][j][k][l] &= 0xFFFFFFFE;
                value = message[count/8];
                value = (value >> (7-(count%8))) & 1;
                //printf("%d", value);
                img->dctCoeffs[i][j][k][l] += value;
                //printf("\t\t%d\t->i %d\tj %d\tk %d\tl %d\t%d\n",alea,i,j,k,l,count);
                count ++;
            }
        }
        else {
            img->dctCoeffs[i][j][k][l] &= 0xFFFFFFFE;
            value = message[count/8];
            value = (value >> (7-(count%8))) & 1;

            //printf("%d", value);
            img->dctCoeffs[i][j][k][l] += value;
            //printf("\t\t%d\t->i %d\tj %d\tk %d\tl %d\t%d\n",alea,i,j,k,l,count);
            count ++;
        }
    }
    printf("Taille du texte : \t\t%d\nNombre de DCT modifiable : \t%d\nNombre de DCT total : \t\t%d\nNombre de DCT modifé : \t\t%d/%d\nTaux d'insertion : \t\t%f\n",
            (message[0]<<24)+(message[1]<<16)+(message[2]<<8)+message[3], nb_dct, nb_dct_total, count, nb_dct, (double)count/nb_dct);
    free(message);
    free(tab_alea);
    return 0;
}
