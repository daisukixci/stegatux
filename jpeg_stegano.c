#include "jpeg_stegano.h"
#include "insertion.h"

/*********************** init_jpeg_img() ********************************
Memory allocation and initialisation of a JPEGimg structr
Returns a pointersto the struct if correctly allocated,
        NULL in case of error

ARGS: (void)
*************************************************************************/
JPEGimg * init_jpeg_img ( void ) {
	JPEGimg * img = NULL;
	if ( (img = (JPEGimg*) malloc (sizeof(JPEGimg)) ) == NULL)
	{
		print_err ("init_jpeg_img", "img", ERR_MEM);
		return NULL;
	}
	img->dctCoeffs = NULL;
	img->virtCoeffs = NULL;
	
	return img;
}

/************************* free_jpeg_img() *****************************
Free memory
Returns EXIT_SUCCESS
        a negative value in case of failure

ARGS: JPEGimg *img: structure to free
************************************************************************/
int free_jpeg_img ( JPEGimg *img ) {
	// Checkargs
	if (!img)
		return ERR_ARG;
	
	// Free memory
	if (img->dctCoeffs)
	{
		free (img->dctCoeffs);
		img->dctCoeffs = NULL;
	}
	
	jpeg_destroy_decompress(&(img->cinfo));
	free(img);
	img = NULL;
	
	return EXIT_SUCCESS;
}


/************************* jpeg_read() **********************************
Reads a jpeg image, store information and DCT coefficients
Returns a pointer to the structure created,
        NULL in case of failure
Retourne un code d'erreur

ARGS: char *path:       Path of the image
*********************************************************************/
JPEGimg * jpeg_read (Option *options)
{
    int comp;
    struct jpeg_error_mgr jerr; // Handle errors
    FILE *infile = NULL;
    JPEGimg *img = NULL;

    // Check args
    if (!options->input)
    {
        print_err ("jpeg_read()", "path", ERR_ARG);
        return NULL;
    }

    // Open path
    if ((infile = fopen(options->input, "rb") ) == NULL)
    {
        print_err ("jpeg_read()", options->input, ERR_FOPEN);
        return NULL;
    }

    // Memory allocation for img
    if ((img = init_jpeg_img()) == NULL)
    {
        fclose (infile);
        return NULL;
    }

    /* Initialize the JPEG decompression object with default error handling. */
    img->cinfo.err = jpeg_std_error (&jerr);
    jpeg_create_decompress (&(img->cinfo));

    /* Specify data source for decompression */
    jpeg_stdio_src (&(img->cinfo), infile);

    // Read header
    (void) jpeg_read_header (&(img->cinfo), TRUE);

    /* Get DCT coefficients
     * dct_coeffs is a virtual array of the components Y, Cb, Cr
     * access to the physical array with the function
     * (cinfo->mem -> access_virt_barray)*/
    img->virtCoeffs = jpeg_read_coefficients (&(img->cinfo));

    // Structure allocation
    img->dctCoeffs = (JBLOCKARRAY*) malloc (sizeof(JBLOCKARRAY) * img->cinfo.num_components );
    if (img->dctCoeffs == NULL)
    {
        print_err ("jpeg_read()", "img->dctCoeffs", ERR_MEM);
        fclose (infile);
        return NULL;
    }

    // Loop on the components of the virtual array to get DCT coefficients
    for(comp = 0; comp < img->cinfo.num_components; comp++)
    {
        img->dctCoeffs[comp] = (img->cinfo.mem -> access_virt_barray)((j_common_ptr) &(img->cinfo),
                img->virtCoeffs[comp], 0, 1, TRUE);
    }

    /*
     * Extract data if options are into extract mode
     */
    if (options->mode == EXTRACT_MODE) { 
        if (options->alea_mode == ALEA_MODE) { 
            printf("Begining of data extraction with key\n");
            extract_lsb_data_with_key(img, options);
            printf("Data extracted\n");
        }
        else {
            printf("Begining of data extraction\n");
            extract_lsb_data(img, options);
            printf("Data extracted\n");
        }
    }
    else if (options->mode == HISTO_MODE){
    	write_histo(img);
    }

    // free and close
    fclose (infile);

    return img;
}

int write_histo (JPEGimg *img){
	int i,j,k,l,m;
	int retour, min, max, nb_dct = 0;
	unsigned int *tab_histo = NULL;
	FILE *histo= NULL;
	
	if(min > max)
    {
          printf("ERROR - Invalid value for minv and maxv : minv > maxv");
          return EXIT_FAILURE;
    }
	
	printf("saisser la valeur minimal de l'histogramme (valeur entière):");
	retour = scanf("%d",&min);
	
	printf("saisser la valeur maximal de l'histogramme (valeur entière):");
	retour = scanf("%d",&max);
	
	for(i=0;i<3;i++){
		nb_dct += img->cinfo.comp_info[i].height_in_blocks * img->cinfo.comp_info[i].width_in_blocks * 64;
	}
	//création du tableau ou vont être stockée les valeur de l'histogramme
	if( (tab_histo = (unsigned int *)calloc(max - min, sizeof(unsigned int))) == NULL )
		printf("error malloc tab_size\n");
	
	for (i=0; i<3; i++){
		for (j=0; j<img->cinfo.comp_info[i].height_in_blocks; j++){
			for (k=0; k<img->cinfo.comp_info[i].width_in_blocks; k++){
				for (l=0; l<64; l++){
					if(img->dctCoeffs[i][j][k][l] >= min && img->dctCoeffs[i][j][k][l] <= max){
						tab_histo[ img->dctCoeffs[i][j][k][l] - min]++;
					}
				}
			}
		}
	}
	
	histo = fopen("histo.csv","w");
	
    fprintf(histo, "\"DCT coef.\";\"nb coef\"\n");
    for(i = min; i <= max; i++)
    {
         fprintf(histo, "%d;%d\n", i, tab_histo[i - min]);
    }

    fclose(histo);
    free(tab_histo);
    
    printf("histo.csv file written.\n");
	return EXIT_SUCCESS;
}

/******************** jpeg_write_from_coeffs() **************************
  Write a jpeg image from its DCT coeffs
  Returns EXIT_SUCCESS, 
  a negative value in case of error

ARGS: char *outfile: Path of the new image
JPEGimg *img:  Structure containing DCT coeffs and infos of the image
 ************************************************************************/
int jpeg_write_from_coeffs (Option *options , JPEGimg *img)
{
    struct jpeg_compress_struct cinfo;
    struct jpeg_error_mgr jerr;
    FILE *output = NULL;

    // Open file
    if ((output = fopen (options->output, "wb")) == NULL)
    {
        print_err( "jpeg_write_from_coeffs()", options->output, ERR_FOPEN);
        return ERR_FOPEN;
    }

    printf("Proper mode is %d - alea mode is %d\n", options->proper_mode, options->alea_mode);

    if(options->alea_mode == ALEA_MODE){
        if(insertion_cle(img, options->proper_mode,options->rate_mode) == 1)
            printf("insertion impossible\n");
    }
    else {
        if(insertion(img, options->proper_mode,options->rate_mode) == 1)
            printf("insertion impossible\n");
    }

    /* Initialize the JPEG compression object with default error handling. */
    cinfo.err = jpeg_std_error(&jerr);
    jpeg_create_compress(&cinfo);

    /*telling, where to put jpeg data*/
    jpeg_stdio_dest(&cinfo, output);

    /* Applying parameters from source jpeg */
    jpeg_copy_critical_parameters(&(img->cinfo), &cinfo);

    /* copying DCT */
    jpeg_write_coefficients(&cinfo, img->virtCoeffs);

    /*clean-up*/
    jpeg_finish_compress(&cinfo);
    jpeg_destroy_compress(&cinfo);
    fclose (output);

    /*Done!*/
    return EXIT_SUCCESS;
}

/***************************** main() ***********************************
  Main function (see help for more details)
 ************************************************************************/
int main (int argc, char ** argv)
{
    int i; // Loop variable
    int return_value;
    JPEGimg *img = NULL;
    Option *options = init_options();

    printf("Init option\n");
    init_options(options);
    option_parser(argc, argv, options);

    srand((int)time(NULL));

    printf("Read the jpeg file\n");
    // read image and ...
    img = jpeg_read (options);

    if (options->mode == STEGANO_MODE) { 
        // ... write it in a new file
        printf("Beginning of data insertion\n");
        return_value = jpeg_write_from_coeffs (options,img);
        if (return_value == EXIT_SUCCESS)
            printf ("Image written in %s\n", argv[2]);	
    }

    free_jpeg_img ( img );

    return EXIT_SUCCESS;
}
