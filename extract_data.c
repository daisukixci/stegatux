#include "extract_data.h"

void extract_lsb_data(JPEGimg *img, Option *options) {
    int i, j, k, l, m;
    int pos_of_data = 0;
    int size = 0, count = 0;
    unsigned char *data = NULL;

    /*
     * Read data from the JPEGimg
     */
    for (i = 0; i < NB_COMPONENT; i++) {
        for (j = 0; j < img->cinfo.comp_info[i].height_in_blocks; j++) {
            for (k = 0; k < img->cinfo.comp_info[i].width_in_blocks; k++) {
                for (l = 1; l < NB_DCT_ELEMENT; l++) {
                    /*
                     * Read size of data in the picture
                     */
                    if (options->proper_mode == PROPER_MODE) {
                        if(img->dctCoeffs[i][j][k][l] != 0 && img->dctCoeffs[i][j][k][l] != 1 /*&& img->dctCoeffs[i][j][k][l] != -1*/) {
                            if (count < 32) {

                                size = size << 1;
                                size += img->dctCoeffs[i][j][k][l] & 1;

                                if (count == 31) { 
                                    printf("Data size: %d\n", size);
                                    data = malloc(size*sizeof(char));
                                }
                            }
                            else {
                                data[pos_of_data] = data[pos_of_data] << 1;
                                data[pos_of_data] += img->dctCoeffs[i][j][k][l] & 1;
                                if (count%8 == 7) { 
                                    pos_of_data ++;
                                }
                            }
                            count ++;

                            if (pos_of_data == size && size != 0) { 
                                printf("Data writing ...\n");
                                write_in_file(data, size, "./data_extracted_proper");
                                free(data);
                                return;
                            }
                        }
                    }
                    else {
                        if (count < 32) { 

                            size = size << 1;
                            size += img->dctCoeffs[i][j][k][l] & 1;
                            if (count == 31) { 
                                printf("Data size: %d\n", size);
                                data = malloc(size*sizeof(char));
                            }
                        }
                        else {
                            data[pos_of_data] = data[pos_of_data] << 1;
                            data[pos_of_data] += img->dctCoeffs[i][j][k][l] & 1;
                            if (count%8 == 7) { 
                                pos_of_data ++;
                            }
                        }
                        count ++;

                        if (pos_of_data == size && size != 0) { 
                            printf("Data writing ...\n");
                            write_in_file(data, size, "./data_extracted");
                            free(data);
                            return;
                        }
                    }
                }
            }
        }
    }
}

void extract_lsb_data_with_key(JPEGimg *img, Option *options) {
    int i, j, k, l, m;
    int pos_of_data = 0;
    int size = 0, count = 0, count_pos = 0;
    int retour, nb_dct_total = 0, valid = 0, alea;
    unsigned int cle = 0;
    unsigned int *tab_size = NULL;
	unsigned int *tab_alea = NULL;
    unsigned char *data = NULL;

    for(i=0;i<3;i++){
        nb_dct_total += img->cinfo.comp_info[i].height_in_blocks * img->cinfo.comp_info[i].width_in_blocks * 64;
    }

    //saisi de la cle
    printf("type the key (only positive numbers) : ");
    retour = scanf("%d", &cle);

    //initialisation de la suite aleatoire en fonction de la cle
    srand(cle);
    
    //initialisation du tableau contenant les positions des dct modifié afin d'éviter les ecrasements de dne
    if( (tab_size = malloc(32*sizeof(unsigned int))) == NULL )
        printf("error malloc tab_size\n");
        
    count = 0;
    while(count < 32){
        
        //verification anti doublon
        do{
        	valid = 0;
            alea = rand()%(nb_dct_total+1);
            for(m = 0; m < count; m++)
                if(tab_size[m] == alea)
                    valid++;
        }while(valid > 0);
        tab_size[count] = alea;
        //definition de la position par rapport a la valeur aléatoire
        count_pos = 0;
        for (i=0; i<3; i++){
			for (j=0; j<img->cinfo.comp_info[i].height_in_blocks; j++){
				for (k=0; k<img->cinfo.comp_info[i].width_in_blocks; k++){
					for (l=0; l<64; l++){
						if(count_pos == alea){
							goto out1;
						}
						count_pos ++;
					}
				}
			}
		}
        out1:
        //printf("%d -- %d\n", alea, img->dctCoeffs[i][j][k][l]);

        if (options->proper_mode == PROPER_MODE) {
            if(img->dctCoeffs[i][j][k][l] != 0 && img->dctCoeffs[i][j][k][l] != 1 /*&& img->dctCoeffs[i][j][k][l] != -1*/ && l != 0) {
                size = size << 1;
                size += img->dctCoeffs[i][j][k][l] & 1;
                //printf("%d", size&1);
                //printf("\t\t%d\t->i %d\tj %d\tk %d\tl %d\n",alea,i,j,k,l);
                count++;
            }
        }
        else {
            size = size << 1;
            size += img->dctCoeffs[i][j][k][l] & 1;
            //printf("%d", size&1);
            //printf("\t\t%d\t->i %d\tj %d\tk %d\tl %d\n",alea,i,j,k,l);
            count ++;
        }
    }
    printf("Data size: %d\n", size);
    data = malloc(size *sizeof(char));
    
    //initialisation du tableau contenant les positions des dct modifié afin d'éviter les ecrasements de dne
    if( (tab_alea = malloc(size*8*sizeof(unsigned int))) == NULL )
        printf("error malloc tab_alea\n");

	count = 0;
    while (count < (size)*8) {
        //verification anti doublon
        do{
        	valid = 0;
            alea = rand()%(nb_dct_total+1);
            for(m = 0; m < 32; m++)
                if(tab_size[m] == alea)
                    valid++;
            for(m = 0; m < count; m++)
                if(tab_alea[m] == alea)
                    valid++;
        }while(valid > 0);
        tab_alea[count] = alea;
        //definition de la position par rapport a la valeur aléatoire
        count_pos = 0;
        for (i=0; i<3; i++){
			for (j=0; j<img->cinfo.comp_info[i].height_in_blocks; j++){
				for (k=0; k<img->cinfo.comp_info[i].width_in_blocks; k++){
					for (l=0; l<64; l++){
						if(count_pos == alea){
							goto out2;
						}
						count_pos ++;
					}
				}
			}
		}
		out2:
		//printf("%d -- %d\n", alea, img->dctCoeffs[i][j][k][l]);
		
        if (options->proper_mode == PROPER_MODE) {
            if(img->dctCoeffs[i][j][k][l] != 0 && img->dctCoeffs[i][j][k][l] != 1 /*&& img->dctCoeffs[i][j][k][l] != -1*/ && l != 0){
                data[pos_of_data] = data[pos_of_data] << 1;
                data[pos_of_data] += img->dctCoeffs[i][j][k][l] & 1;
                if (count%8 == 7) {
                    pos_of_data ++;
                }
                //printf("%d", img->dctCoeffs[i][j][k][l] & 1);
                //printf("\t\t%d\t->i %d\tj %d\tk %d\tl %d\n",alea,i,j,k,l);
                count++;
            }
        }
        else {
            data[pos_of_data] = data[pos_of_data] << 1;
            data[pos_of_data] += img->dctCoeffs[i][j][k][l] & 1;
            if (count%8 == 7) { 
                pos_of_data ++;
            }
            //printf("%d", img->dctCoeffs[i][j][k][l] & 1);
            //printf("\t\t%d\t->i %d\tj %d\tk %d\tl %d\n",alea,i,j,k,l);
            count ++;
        }
    }
	printf("Data writing ...\n");
    write_in_file(data, size, "./data_extracted_key");
    free(data);
    free(tab_alea);
    free(tab_size);
}
